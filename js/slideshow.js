(function() {
  'use strict';
//is master
//
  var slideshow = [{
    "image": "images/gallery_1.jpeg",
    "caption": "Cloudy with a chance of moon"
  }, {
    "image": "images/gallery_2.jpeg",
    "caption": "Half moon mountain"
  }, {
    "image": "images/gallery_3.jpeg",
    "caption": "Moonrise"
  }];
  var index = 0;
    for (var i = slideshow.length - 1; i >= 0; i--) { 
        $('.gallery-wrapper').append('<img id=img_'+i+' src="'+slideshow[i]['image']+'" alt="'+slideshow[i]['caption']+'" title="'+slideshow[i]['caption']+'" >');
       if(index == i) 
        {
          setCurrent(index);
        }
    }
  $('#prev').attr('disabled','disabled');
  
  function setCurrent(index) {
    $('#gallery .gallery-wrapper img').removeClass('current');
    $('#img_'+index).addClass('current');
    $('#caption').text(slideshow[index]['caption']);
  }

  $('#next').click(function()
  {
    index++;
    if(index<=(slideshow.length-1))
    {
      setCurrent(index);
      $('#prev').removeAttr('disabled');
    }
    if(index==(slideshow.length-1))
      $('#next').attr('disabled','disabled');
    
  })
  $('#prev').click(function()
  {
    index--;
    if(index>=0)
    {
      setCurrent(index);
      $('#next').removeAttr('disabled');
    }
    if(index==0)
      $('#prev').attr('disabled','disabled');
    
  })
})();
