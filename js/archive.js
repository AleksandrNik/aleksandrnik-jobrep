(function() {
    //

    // for master

  var jsonURL = 'https://credentials-api.generalassemb.ly/explorer/posts';
  var offset = 0;
    $('#load-more').click(function () {
        var buttonLoadMore = $(this);
        $.ajax({
            url: jsonURL,
            data: {offset: offset},
            dataType: 'json',
            success: function (data) {
                if (data['posts'].length > 0) {
                    buttonLoadMore.html('Load more');
                    buttonLoadMore.attr('disabled',false);
                    var i = 0;
                    while (i < 4) {
                        $('#article-list').append('<article>' +
                            '<i class="' + data['posts'][i]['category'] + '"></i>' +
                            '<h2>From the Archive </h2>' +
                            '<h1>' + data['posts'][i]['title'] + '</h1>' +
                            '<h3>' + data['posts'][i]['date'] + '</h3>' +
                            '<p>' + data['posts'][i]['blurb'] + '</p>' +
                            '</article>');
                        i++;
                    }
                    buttonLoadMore.parent('footer:first').appendTo('#article-list');
                    offset += 4;
                }
                else {
                    buttonLoadMore.attr('disabled',true);
                    buttonLoadMore.html('End of Archive');
                }
            },
            beforeSend: function () {
                buttonLoadMore.attr('disabled',true);
                buttonLoadMore.html('Exploring the archive ... <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            error: function () {
                buttonLoadMore.attr('disabled',true);
                buttonLoadMore.html('Something Went Wrong <i class="fa fa-exclamation-triangle"></i>');
            },
        });
    });
})();
